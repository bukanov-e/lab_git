#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <Windows.h>

using namespace std;

template <typename type> class Predicate:private unary_function<type, bool>
{
private:
	type local;
public:
	Predicate( type a )
	{
		local = a;
	}
    bool operator()(const type &a) const
    {
		return a == local;
    }
};


int main()
{
    vector<char> str;
    str.push_back( '1' );
    str.push_back( '2' );
    str.push_back( '3' );
    str.push_back( '4' );
    str.push_back( '5' );
	str.push_back( '6' );
	str.push_back( '7' );
	str.push_back( '8' );
	str.push_back( '9' );
	str.push_back( '0' );


	char sym;
	cout<< "Input wanted symbol: ";
	cin>> sym;

	vector<char>::iterator tmp = std::find_if( str.begin(), str.end(), Predicate<char>(sym) );
	if( tmp != str.end() )
		cout<< "Find sym at: "<< tmp - str.begin() + 1<< " position."<< endl;
	else
		cout<< "Don't find sym."<< endl;

	system("pause");
    return 0;
}
