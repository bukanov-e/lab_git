#include <cstdint>

#include <functional>
#include <algorithm>
#include <iostream>
#include <vector>


#define LEN 15

std::vector<bool> get_sieve( size_t n ) {
	std::vector<bool> S = std::vector<bool>( 2*n, true );
	S[0] = 0;
	S[1] = 0;

	for ( uint32_t k = 2; k*k <= n; k++ )
	{
		if ( S[k] == true )
		{
			for ( uint32_t l = k*k; l <= n; l += k )
			{
				S[l] = false;
			}
		}
	}
	return S;
}

int main()
{
	std::function<bool(int)> is_primitive = []( int num )
	{
		uint32_t anum = num >= 0 ? num : -num;

		static size_t sieve_size = 0;
		static std::vector<bool> sieve;

		if ( anum > sieve_size )
		{
			sieve = get_sieve( anum );
			sieve_size = anum;
		}

		return sieve[anum];
	};

	std::vector<int> v(LEN);
	for ( size_t i = 0; i < LEN; ++i )
	{
		v[i] = LEN - i;
	}

	auto primitive = std::find_if(v.begin(), v.end(), is_primitive);

	std::cout << "First primitive element has value " << *primitive << std::endl;

	return 0;
}